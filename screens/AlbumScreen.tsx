import React,{useEffect} from 'react'
import { StyleSheet, Text, View,FlatList } from 'react-native'
import { useRoute } from '@react-navigation/native';
import SongListItem from '../components/SongListItem';
import albumDetails from '../data/albumDetails'
import AlbumHeader from '../components/AlbumHeader'
const AlbumScreen = () => {
    
  const route = useRoute();
  const albumId = route.params.id;
   useEffect(() => {
   console.log(route);
  }, [])
    return (
        <View>
            <Text style={{color:'white'}}>Hello from album Screen</Text>
             <FlatList
        data={albumDetails.songs}
        renderItem={({ item }) => <SongListItem song={item} />}
        keyExtractor={(item) => item.id}
        ListHeaderComponent={() => <AlbumHeader album={albumDetails} />}
      />
        </View>
    )
}

export default AlbumScreen

const styles = StyleSheet.create({})
