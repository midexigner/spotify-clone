import * as React from 'react';
import { StyleSheet,Text,FlatList, View } from 'react-native';
import AlbumCategory from '../components/AlbumCategory';
import categories from '../data/albumCategories'
export default function TabOneScreen() {
  return (
    <View style={styles.container}>
   <FlatList
         data={categories}
         renderItem={({ item }) => (
           <AlbumCategory
             title={item.title}
             albums={item.albums}
           />
         )}
         keyExtractor={(item) => item.id}
       />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  
});
